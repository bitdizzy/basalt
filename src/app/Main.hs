{-# LANGUAGE TemplateHaskell #-}
module Main where

import Data.Foldable
import Data.Functor.Identity
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import qualified Data.Set as Set
import Development.GitRev
import Options.Applicative
import System.Directory
import System.FilePath

import Basalt

main :: IO ()
main = do
  defConf <- defaultBasaltConfig
  execBasalt . optionsToBasaltEnv defConf =<< detectTarget =<< execParser opts

opts :: ParserInfo (Options Maybe)
opts = info (helper <*> version <*> options)
  ( fullDesc
    <> progDesc "Version control for Nix configurations"
    <> header "basalt command line application"
  )

version :: Parser (a -> a)
version = infoOption (concat ["basalt ", $(gitBranch), " @ ", $(gitHash)])
                     (long "version" <> short 'V' <> help "Show version information")

options :: Parser (Options Maybe)
options = Options
  <$> subparser
    ( command "switch" (info (helper <*> parseSwitchOptions) (progDesc "Build and switch to a new configuration"))
    <> command "build" (info (helper <*> parseBuildOptions) (progDesc "Build a commit as an immediately switchable configuration"))
    <> command "update" (info (helper <*> parseUpdateOptions) (progDesc "Update and commit top-level thunks"))
    )
  <*> optional (option parseTarget (long "target" <> metavar "TARGET" <> help "nixos or home-manager"))
  <*> optional (strOption $ long "root" <> metavar "ROOT" <> help "root directory to build configuration in")

data Options t = Options
  { optionsCommand :: Command
  , optionsTarget  :: t Target
  , optionsRoot    :: Maybe FilePath
  }

parseTarget :: ReadM Target
parseTarget = eitherReader $ \t -> case t of
  "nixos" -> Right NixOS
  "home-manager" -> Right HomeManager
  _ -> Left "Invalid value for 'target'. Use 'nixos' or 'home-manager'"

detectTarget :: Options Maybe -> IO (Options Identity)
detectTarget opts' = case optionsTarget opts' of
  Just target -> pure $ opts' { optionsTarget = pure target }
  Nothing -> do
    currentDir <- getCurrentDirectory
    (,) <$> (doesFileExist $ currentDir </> "configuration.nix")
        <*> (doesFileExist $ currentDir </> "home.nix") >>= \case
      (True, False) -> do
        putStrLn "Basalt target missing, assuming NixOS"
        pure $ opts' { optionsTarget = pure NixOS }
      (False, True) -> do
        putStrLn "Basalt target missing, assuming HomeManager"
        pure $ opts' { optionsTarget = pure HomeManager }
      (False, False) -> error "Failed to find relevant .nix files, please specify --target parameter"
      (True, True) -> error "configuration.nix and home.nix exist, please specify --target parameter to clarify which you want to use"

data Command = BuildCommand BuildOptions | SwitchCommand SwitchOptions | UpdateCommand (Maybe (NonEmpty FilePath))

data BuildOptions = BuildOptions { _buildOptions_test :: !Bool }
data SwitchOptions = SwitchOptions { }

parseBuildOptions :: Parser Command
parseBuildOptions = BuildCommand <$> BuildOptions <$> switch (long "test" <> help "Test build the current worktree instead of building and recording a commit")

parseSwitchOptions :: Parser Command
parseSwitchOptions = pure $ SwitchCommand SwitchOptions

parseUpdateOptions :: Parser Command
parseUpdateOptions = fmap (UpdateCommand . sanitize) $ many $ strOption $ fold
  [ action "directory"
  , metavar "DIR"
  , long "thunk"
  , help "Specify a thunk to be updated - can be supplied multiple times. If none is specified, all thunks in the current directory will be bumped"
  ]
  where
    sanitize = nonEmpty . toList . Set.fromList

optionsToBasaltEnv :: BasaltCliConfig -> Options Identity -> BasaltEnv
optionsToBasaltEnv defConfig (Options cmd (Identity target) root) = case cmd of
  BuildCommand (BuildOptions True) -> BasaltEnv {
    basaltEnvAction = TestBuild
  , basaltEnvTarget = target
  , basaltEnvCliConfig = defConfig
  , basaltEnvRoot = root
  }
  BuildCommand (BuildOptions False) -> error "Prebuilding is currently unsupported, use 'basalt switch' for now"
  SwitchCommand _ -> BasaltEnv {
    basaltEnvAction = Switch
  , basaltEnvTarget = target
  , basaltEnvCliConfig = defConfig
  , basaltEnvRoot = root
  }
  UpdateCommand thunks -> BasaltEnv {
    basaltEnvAction = (Update thunks)
  , basaltEnvTarget = target
  , basaltEnvCliConfig = defConfig
  , basaltEnvRoot = root
  }
