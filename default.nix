let
  obelisk = import ./dep/obelisk {};
  obelisk-command-overlay = (obelisk.reflex-platform.hackGet ./dep/obelisk) + "/haskell-overlays/obelisk.nix";

  gitignore = obelisk.nixpkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore";
    rev = "f9e996052b5af4032fe6150bba4a6fe4f7b9d698";
    sha256 = "0jrh5ghisaqdd0vldbywags20m2cxpkbbk5jjjmwaw0gr8nhsafv";
  };
  gitignoreSource = (import gitignore {}).gitignoreSource;

  # Define GHC compiler override
  pkgs = obelisk.obelisk.override {
    overrides = self: super: with obelisk.nixpkgs; rec {
      obelisk-command = (import obelisk-command-overlay self super).obelisk-command;
      basalt = haskell.lib.overrideCabal (pkgs.callCabal2nix "basalt" (gitignoreSource ./.) {}) (o: {
        executableToolDepends = (o.executableToolDepends or []) ++ [ git nix nix-prefetch-git ];
      });
      # which = self.callHackage "which" "" {};
    };
  };
in {
  inherit pkgs;
  inherit (pkgs) basalt;
}
