{ config, pkgs, lib, ... }:
let qemuConfig = (import (pkgs.path + "/nixos/modules/virtualisation/qemu-vm.nix") {
      inherit config pkgs lib;
    }).config;
    testInstrumentation = (import (pkgs.path + "/nixos/modules/testing/test-instrumentation.nix") {
      inherit config pkgs lib;
    }).config;
in {
  imports = [
    ./hardware-configuration.nix
  ];
  inherit (testInstrumentation) boot networking users;

  time.timeZone = "America/New_York";

  environment.systemPackages = with pkgs; [
    wget
    # hello
    git
    gnutar
    gnused
    bash
    unixtools.ping
  ];

  # Add reflex-frp cache
  nix.binaryCaches = [ "https://cache.nixos.org/" "https://nixcache.reflex-frp.org" ];
  nix.binaryCachePublicKeys = [ "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=" ];

  # TODO: This will be necessary for sandboxing
  # nix.binaryCaches = lib.mkForce [];

  services = { 
    inherit (qemuConfig.services) timesyncd connman;
    inherit (testInstrumentation.services) journald;
    qemuGuest.enable = true;
  };
  # source: ./nixpkgs/nixos/modules/virtualisation/qemu-vm.nix
  services.xserver.videoDrivers = [ "modesetting" ];
  services.xserver.defaultDepth = 0;
  services.xserver.resolutions = [ { x = 1024; y = 768; } ];
  services.xserver.monitorSection =
    ''
      # Set a higher refresh rate so that resolutions > 800x600 work.
      HorizSync 30-140
      VertRefresh 50-160
    '';

  # source: ./nixpkgs/nixos/modules/testing/test-instrumentation.nix
  services.xserver.displayManager.job.logToJournal = true;


  system.requiredKernelConfig = qemuConfig.system.requiredKernelConfig ++ testInstrumentation.system.requiredKernelConfig;

  systemd.services = testInstrumentation.systemd.services // {
    backdoor = testInstrumentation.systemd.services.backdoor // {
      reloadIfChanged = false;
      restartIfChanged = false;
      stopIfChanged = false;
    };
  };
  systemd.extraConfig = testInstrumentation.systemd.extraConfig;
}
