# NixOS-style tests

NB: Currently these tests cannot be run in an automated
way (i.e., via nix-build).

## Running the tests interactively

```
$(nix-build -A driver)/bin/nixos-test-driver
test_script()
```
